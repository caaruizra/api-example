package itm.cs.clase4.integration;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import itm.cs.clase4.controllers.ClientController;
import itm.cs.clase4.model.Client;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClientControllerIntegrationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ClientController controller;

	@Autowired
	private TestRestTemplate restTemplate;

	@AfterEach
	void tearDown() {
		JdbcTestUtils.deleteFromTables(jdbcTemplate, "client");
	}

	@Test
	public void smokeTest() throws Exception {
		assertThat(controller).isNotNull();
	}

	@Sql(scripts = "/data_test.sql")
	@Test
	public void testGetAllClientsAndCheckEquals2() throws Exception {
		List<Client> listOfClients = this.restTemplate.getForObject("http://localhost:" + port + "/api/client",
				List.class);
		assertNotNull(listOfClients);
		assertEquals(listOfClients.size(), 2);
	}

	@Sql(scripts = "/data_test.sql")
	@Test
	public void testDeleteClientsAndCheckEquals1() throws Exception {
		List<Client> listOfClients = this.restTemplate.getForObject("http://localhost:" + port + "/api/client",
				List.class);
		assertNotNull(listOfClients);
		assertEquals(listOfClients.size(), 2);
		this.restTemplate.delete("http://localhost:" + port + "/api/client/1");
		listOfClients = this.restTemplate.getForObject("http://localhost:" + port + "/api/client", List.class);
		assertNotNull(listOfClients);
		assertEquals(listOfClients.size(), 1);
	}

	@Sql(scripts = "/data_test.sql")
	@Test
	public void testDeleteNotExistingClient() throws Exception {
		List<Client> listOfClients = this.restTemplate.getForObject("http://localhost:" + port + "/api/client",
				List.class);
		assertNotNull(listOfClients);
		assertEquals(listOfClients.size(), 2);
		this.restTemplate.delete("http://localhost:" + port + "/api/client/100");
		listOfClients = this.restTemplate.getForObject("http://localhost:" + port + "/api/client", List.class);
		assertNotNull(listOfClients);
		assertEquals(listOfClients.size(), 2);
	}

	@Sql(scripts = "/data_test.sql")
	@Test
	public void testCreateClient() throws Exception {
		Client c = new Client();
		c.setFirstname("Bart");
		c.setLastname("Simpson");
		c.setPhone("3142520");
		c.setAddress("Calle 12 # 34 - 56");

		ResponseEntity<Client> response = this.restTemplate.postForEntity("http://localhost:" + port + "/api/client", c,
				Client.class);
		assertNotNull(response);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getId()).isNotNull();
		assertThat(response.getBody().getFirstname()).isEqualTo("Bart");
		assertThat(response.getBody().getLastname()).isEqualTo("Simpson");
		assertThat(response.getBody().getPhone()).isEqualTo("3142520");
		assertThat(response.getBody().getAddress()).isEqualTo("Calle 12 # 34 - 56");

	}

	@Test
	public void testUpdateClient() throws Exception {
		// Iniciamos el repositorio con un cliente inicial
		Client clientOriginal = new Client();
		clientOriginal.setFirstname("Bart");
		clientOriginal.setLastname("Simpson");
		clientOriginal.setPhone("3142520");
		clientOriginal.setAddress("Calle 12 # 34 - 56");

		ResponseEntity<Client> responseCreate = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/client", clientOriginal, Client.class);

		Client createdClient = responseCreate.getBody();
		createdClient.setFirstname("Bartolomeo");
		assertNotNull(createdClient);
		System.out.println(createdClient.getId());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Client> request = new HttpEntity<>(createdClient, headers);
		ResponseEntity<Client> response = restTemplate.exchange(
				"http://localhost:" + port + "/api/client/" + createdClient.getId(), HttpMethod.PUT, request,
				Client.class);

		assertNotNull(response);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getId()).isEqualTo(createdClient.getId());
		assertThat(response.getBody().getFirstname()).isEqualTo("Bartolomeo");
		assertThat(response.getBody().getLastname()).isEqualTo("Simpson");
		assertThat(response.getBody().getPhone()).isEqualTo("3142520");
		assertThat(response.getBody().getAddress()).isEqualTo("Calle 12 # 34 - 56");

	}

	@Test
	public void testGetClient() throws Exception {
		// Iniciamos el repositorio con un cliente inicial
		Client clientOriginal = new Client();
		clientOriginal.setFirstname("Bart");
		clientOriginal.setLastname("Simpson");
		clientOriginal.setPhone("3142520");
		clientOriginal.setAddress("Calle 12 # 34 - 56");

		ResponseEntity<Client> responseCreate = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/client", clientOriginal, Client.class);

		ResponseEntity<Client> response = this.restTemplate
				.getForEntity("http://localhost:" + port + "/api/client/"+responseCreate.getBody().getId(),  Client.class);

		assertNotNull(response);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getFirstname()).isEqualTo("Bart");
		assertThat(response.getBody().getLastname()).isEqualTo("Simpson");
		assertThat(response.getBody().getPhone()).isEqualTo("3142520");
		assertThat(response.getBody().getAddress()).isEqualTo("Calle 12 # 34 - 56");

	}

}
