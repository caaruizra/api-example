package itm.cs.clase4;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import itm.cs.clase4.controllers.ClientController;

@SpringBootTest
class ItmcsCrudRestApplicationTests {
	@Autowired
	private ClientController controller;
	

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
