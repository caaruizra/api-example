package itm.cs.clase4.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import itm.cs.clase4.controllers.ClientController;
import itm.cs.clase4.model.Client;
import itm.cs.clase4.services.IClientService;
import jakarta.annotation.Resource;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClientUnitTest {
	

	@Mock
	private IClientService clientService;
    
	@InjectMocks
	@Resource
 	private ClientController clientController;
 

    

	  @BeforeEach
	  public void setup() {
	    MockitoAnnotations.openMocks(this);
	  }

	  @Test
	  public void testGetClient() {
	    Client client = new Client(1,"Lisa","Simpson","3145201","Avenida Siempre Viva 1234");
	    when(clientService.findById(1l)).thenReturn(client);
	    Client result = clientController.getClient(new HashMap<>(), 1l);
	    assertThat(result).isEqualTo(client);
	  }

}
