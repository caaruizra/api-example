package itm.cs.clase4.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import itm.cs.clase4.model.Client;
import itm.cs.clase4.services.IClientService;

@RestController
@RequestMapping("/api/client")
public class ClientController {
	
	@Autowired
	IClientService clientService;
	
	@GetMapping("")
	public Iterable<Client> getClients(Map<String, Object> model) {
		return clientService.findAll();
	}
	
	@PostMapping("")
	public Client createClient(@RequestBody Client client, Map<String, Object> model) {
			return clientService.save(client);
	}
	
	@GetMapping("/{id}")
	public Client getClient(Map<String, Object> model, @PathVariable Long id) {
		return clientService.findById(id);
	}
	
	
	@DeleteMapping("/{id}")
	public Client deleteClient(Map<String, Object> model, @PathVariable Long id) {
		Client c = clientService.findById(id);
		if(c != null)
			clientService.delete(c);
		return c;
	}
	
	@PutMapping("/{id}")
	public Client updateClient(@RequestBody Client client,@PathVariable Integer id) {
		client.setId(id);
		return clientService.save(client);
	}
}
