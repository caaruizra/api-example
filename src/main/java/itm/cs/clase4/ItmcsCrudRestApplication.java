package itm.cs.clase4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItmcsCrudRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItmcsCrudRestApplication.class, args);
	}

}
