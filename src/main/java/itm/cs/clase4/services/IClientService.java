package itm.cs.clase4.services;


import itm.cs.clase4.model.Client;

public interface IClientService {
	
	public Client findById(Long id);
	public Client save(Client client);
	public void delete(Client client);
	public Iterable<Client> findAll();
}
