package itm.cs.clase4.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import itm.cs.clase4.dao.IClientDao;
import itm.cs.clase4.model.Client;

@Service
public class IClientServiceImplements implements IClientService {

	@Autowired
	IClientDao clientDao;
	
	
	@Override
	public Client findById(Long id) {
		return clientDao.findById(id).orElse(null);
	}

	@Override
	public Client save(Client client) {
		return clientDao.save(client);
	}

	@Override
	public void delete(Client client) {
		clientDao.delete(client);

	}

	@Override
	public Iterable<Client> findAll() {
		return clientDao.findAll();
	}

}
