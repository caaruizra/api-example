package itm.cs.clase4.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import itm.cs.clase4.model.Client;

@Repository
public interface IClientDao extends CrudRepository<Client, Long>  {

}
